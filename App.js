import { AppLoading } from 'expo';
import React, { useState } from 'react';
import AppContainer from './src/navigation/AppContainer';

export default function App() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  return (!isLoadingComplete)
    ? <AppLoading
        onError={e => console.warn(e)}
        onFinish={onFinish(setLoadingComplete)}
      />
    : <AppContainer/>
}

const onFinish = setComplete => setComplete(true);
