import React from 'react';
import Colors from '../../constants/Colors';
import { StyleSheet, Text, View } from 'react-native';

export default class ContactScreen extends React.Component {
  render = () =>
    <View style={styles.contact}>
      <Text>Site web</Text>
      <Text>Email?</Text>
      <Text>Téléphone?</Text>
      <Text>Formulaire de contact</Text>
    </View>
}

// @ts-ignore
ContactScreen.navigationOptions = {
  title: 'Contactez-nous',
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: Colors.navigationBlue,
  headerTitleStyle: {
    fontWeight: 'bold',
    color: Colors.black,
  },
};

const styles = StyleSheet.create({
  contact: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

