import React from 'react';
import { NavigationStackProp } from 'react-navigation-stack'
import Colors from '../constants/Colors';

// @ts-ignore
import LogoSource from '../assets/images/canempire_logo_text.png'
import { Button } from 'react-native-elements';
import { View, StyleSheet, StatusBar } from 'react-native'
import { faCannabis, faComments, faPhone, faCog, faHashtag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import HomeScreenLogo from './HomeScreenLogo';
import { RightSlideAnimation } from '../components/RightSlideAnimation';

interface HomeScreenProps {
  navigation: NavigationStackProp<{ userId: string }>;
}

export default class HomeScreen
  extends React.Component<HomeScreenProps, any> {

  render = () =>
    <View style={styles.container}>
      <StatusBar barStyle='dark-content'/>
      <View style={styles.welcomeContainer}>
        <HomeScreenLogo source={LogoSource} />
      </View>

      <View style={styles.content}>
        <View style={{ flex: 1 }}/>

        <View style={styles.buttonContainer}>
          <View style={{ flex: 19 }}>
            <RightSlideAnimation msDelay={0}>
              <Button
                icon={ <FontAwesomeIcon icon={faComments} size={24} style={styles.buttonIcon} /> }
                title='Revues du pot de la SQDC'
                onPress={() => this.props.navigation.navigate('Review')}
                buttonStyle={styles.button}
              />
            </RightSlideAnimation>
          </View>
          <View style={{ flex: 1 }} />
        </View>

        <View style={styles.buttonContainer}>
          <View style={{ flex: 17 }}>
            <RightSlideAnimation msDelay={150}>
              <Button
                icon={ <FontAwesomeIcon icon={faCannabis} size={24} style={styles.buttonIcon} /> }
                title='CanFest Québec 2020'
                onPress={() => this.props.navigation.navigate('Canfest')}
                buttonStyle={ styles.button }
              />
            </RightSlideAnimation>
          </View>
          <View style={{ flex: 3 }} />
        </View>

        <View style={styles.buttonContainer}>
          <View style={{ flex: 15 }}>
            <RightSlideAnimation msDelay={300}>
              <Button
                icon={ <FontAwesomeIcon icon={faHashtag} size={24} style={styles.buttonIcon} /> }
                title='Médias Sociaux'
                onPress={() => this.props.navigation.navigate('Social')}
                buttonStyle={ styles.button }
              />
            </RightSlideAnimation>
          </View>
          <View style={{ flex: 5 }} />
        </View>

        <View style={styles.buttonContainer}>
          <View style={{ flex: 13 }}>
            <RightSlideAnimation msDelay={450}>
              <Button
                icon={ <FontAwesomeIcon icon={faPhone} size={24} style={styles.buttonIcon} /> }
                title='Contact'
                onPress={() => this.props.navigation.navigate('Contact')}
                buttonStyle={ styles.button }
              />
            </RightSlideAnimation>
          </View>
          <View style={{ flex: 7 }}/>
        </View>
        <View style={styles.buttonContainer}>
          <View style={{ flex: 11 }}>
            <RightSlideAnimation msDelay={600}>
              <Button
                icon={ <FontAwesomeIcon icon={faCog} size={24} style={styles.buttonIcon} /> }
                title='Options'
                onPress={() => this.props.navigation.navigate('Contact')}
                buttonStyle={ styles.button }
              />
            </RightSlideAnimation>
          </View>
          <View style={{ flex: 9 }} />
        </View>
        <View style={{ flex: 1 }}/>
      </View>
    </View>
}

// @ts-ignore
HomeScreen.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.primary,
  },
  content: {
    flex: 1,
  },
  buttonContainer: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    left: -24,
    height: 48,
    paddingLeft: 40,
    backgroundColor: Colors.black,
    justifyContent: 'flex-start',
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 24,
    borderBottomRightRadius: 24,
  },
  buttonIcon: {
    color: Colors.primary,
    marginRight: 12,
  },
  welcomeContainer: {
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  welcomeImage: {
    width: 250,
    height: 200,
    resizeMode: 'contain',
  },
});
