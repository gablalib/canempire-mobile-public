import React from 'react';
import Colors from '../../constants/Colors';
import { StyleSheet, Text, View } from 'react-native';

export default class SocialScreen extends React.Component {
  render = () =>
      <View style={styles.social}>
        <Text>Facebook</Text>
        <Text>Instagram</Text>
        <Text>Spotify</Text>
        <Text>Youtube</Text>
      </View>
}

// @ts-ignore
SocialScreen.navigationOptions = {
  title: 'Suivez-nous',
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: Colors.navigationBlue,
  headerTitleStyle: {
    fontWeight: 'bold',
    color: Colors.black,
  },
};

const styles = StyleSheet.create({
  social: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

