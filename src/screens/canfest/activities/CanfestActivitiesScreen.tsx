import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class CanfestActivitiesScreen extends React.Component<any, any> {
  render = () =>
    <View style={styles.canfestActivities}>
      <Text>Description des festivités</Text>
      <Text>Information générales, etc.</Text>
    </View>
}

// @ts-ignore
CanfestActivitiesScreen.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  canfestActivities: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
