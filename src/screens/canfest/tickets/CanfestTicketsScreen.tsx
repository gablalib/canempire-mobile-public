import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class CanfestTicketsScreen extends React.Component<any, any> {
  render = () =>
    <View style={styles.canfestActivities}>
      <Text>Achat des billets via un lien vers EventBrite</Text>
    </View>
}

// @ts-ignore
CanfestTicketsScreen.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  canfestActivities: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
