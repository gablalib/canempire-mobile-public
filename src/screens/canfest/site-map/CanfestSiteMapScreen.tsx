import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class CanfestSiteMapScreen extends React.Component<any, any> {
  render = () =>
    <View style={styles.canfestSiteMap}>
      <Text>Plan du site</Text>
    </View>
}

// @ts-ignore
CanfestSiteMapScreen.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  canfestSiteMap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
