import React from 'react'
import Colors from '../../constants/Colors';
import { createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import TabBarIcon from '../../components/TabBarIcon';
import CanfestActivitiesScreen from './activities/CanfestActivitiesScreen';
import CanfestScheduleScreen from './schedule/CanfestScheduleScreen';
import CanfestSiteMapScreen from './site-map/CanfestSiteMapScreen';
import CanfestTicketsScreen from './tickets/CanfestTicketsScreen';
import { faMapMarkedAlt, faCalendarAlt, faTicketAlt, faCannabis } from '@fortawesome/free-solid-svg-icons';

const CanfestActivitiesStack = createStackNavigator({ CanfestActivities: CanfestActivitiesScreen });

// @ts-ignore
CanfestActivitiesStack.navigationOptions = {
  tabBarLabel: 'Festivités',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faCannabis} />
  ),
};

const CanfestScheduleStack = createStackNavigator({ CanfestSchedule: CanfestScheduleScreen });

// @ts-ignore
CanfestScheduleStack.navigationOptions = {
  tabBarLabel: 'Horaire',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faCalendarAlt} />
  ),
};

const CanfestSiteMapStack = createStackNavigator({ CanfestSiteMap: CanfestSiteMapScreen });

// @ts-ignore
CanfestSiteMapStack.navigationOptions = {
  tabBarLabel: 'Plan du site',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faMapMarkedAlt} />
  ),
};

const CanfestTicketsStack = createStackNavigator({ CanfestTickets: CanfestTicketsScreen });

// @ts-ignore
CanfestTicketsStack.navigationOptions = {
  tabBarLabel: 'Billets',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faTicketAlt} />
  ),
};

const CanfestTabNavigator = createBottomTabNavigator({
  CanfestActivitiesStack,
  CanfestScheduleStack,
  CanfestTicketsStack,
  CanfestSiteMapStack,
});

const CanfestNavigator = createSwitchNavigator({ Main: CanfestTabNavigator });
CanfestNavigator.navigationOptions = {
  title: 'CanFest QC 2020',
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: Colors.navigationBlue,
  headerTitleStyle: {
    fontWeight: 'bold',
    color: Colors.black,
  },
};

export default CanfestNavigator
