import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class CanfestScheduleScreen extends React.Component {
  render = () =>
    <View style={styles.canfestSchedule}>
      <Text>Grille horaire des festivités</Text>
    </View>
}

const styles = StyleSheet.create({
  canfestSchedule: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

// @ts-ignore
CanfestScheduleScreen.navigationOptions = {
  headerShown: false,
};
