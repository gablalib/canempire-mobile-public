import React from 'react'
import Colors from '../constants/Colors';

import { View, StyleSheet, Animated } from 'react-native';

interface HomeScreenLogoState {
  opacity: Animated.Value
}

export default class HomeScreenLogo
  extends React.Component<any, HomeScreenLogoState> {

  constructor(props: any) {
    super(props)

    this.state = {
      opacity: new Animated.Value(0),
    }
  }

  onLoad = () => {
    Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 480,
      useNativeDriver: true,
    }).start();
  };

  render = () =>
    <View style={styles.logoContainer}>
      <Animated.Image
        onLoad={this.onLoad}
        {...this.props}
        style={{
          ...styles.logo,
          opacity: this.state.opacity,
          transform: [{
            scale: this.state.opacity.interpolate({
              inputRange: [0, 1],
              outputRange: [0.8, 1]
            })
          }],
        }}
      />
    </View>
}

const styles = StyleSheet.create({
  logoContainer: {
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  logo: {
    width: 300,
    height: 240,
    resizeMode: 'contain',
  }
});
