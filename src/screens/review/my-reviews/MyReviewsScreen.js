import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default class MyReviewsScreen extends React.Component {
  render () {
    return (
      <View style={styles.myReviews}>
        <Text>Liste des revues publiées par l'utilisateur actuel</Text>
        <Text>Trier par les plus likées, plus récentes, etc</Text>
        <Text>Système de points/récompenses? (encourager à publier!)</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  myReviews: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

MyReviewsScreen.navigationOptions = {
  headerShown: false,
};
