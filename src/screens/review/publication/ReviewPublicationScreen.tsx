import { CapturedPicture } from 'expo-camera/src/Camera.types'
import React from 'react'
import { ScrollView, StyleSheet, Text, TextInput, View, TouchableOpacity, Dimensions } from 'react-native'
import { faAngleRight, faPencilAlt, faCheckCircle, faTimesCircle, faCamera } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { NavigationStackProp } from 'react-navigation-stack'
import uuidv4 from 'uuid/v4';
import Colors from '../../../constants/Colors';
import ReviewStarPicker from '../../../components/review/ReviewStarPicker';
import { AnimatedCamera } from '../../../components/AnimatedCamera';

interface ReviewPublicationScreenState {
  productText: string
  descriptionText: string
  selectedStars: number
  productInputInvalid: boolean
  productInputDirty: boolean
  cameraImagePicked: boolean
  cameraAllowed: boolean,
  capturedImage: string | null
}

interface ReviewPublicationScreenProps {
  navigation: NavigationStackProp<{ userId: string }>;
}

export default class ReviewPublicationScreen
  extends React.Component<ReviewPublicationScreenProps, ReviewPublicationScreenState> {

  private scrollView: ScrollView = null;

  constructor(props: ReviewPublicationScreenProps) {
    super(props)

    this.state = this.defaultState();
  }

  render = () => (
    <View>
      <View style={styles.header}>
        <Text style={styles.headerTitle}>
          Publier une revue
        </Text>
        <FontAwesomeIcon icon={faPencilAlt} size={28} color={Colors.black} style={styles.headerIcon}/>
      </View>

      <ScrollView
        ref={(view) => {
          this.scrollView = view;
        }}
        contentContainerStyle={styles.reviewPublication}>
        <View style={styles.field}>
          <View style={styles.product}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>
                Produit critiqué
              </Text>
            </View>
            <View style={styles.validationField}>
              <View style={styles.productText}>
                <TextInput
                  ref='product'
                  placeholder='Exemple: Trois et demi, La Batch'
                  onChangeText={productText => this.handleProductInputChange(productText)}
                  maxLength={50}
                  value={this.state.productText}
                  style={this.getInputStyle()}
                />
              </View>
              <View style={styles.productValidation}>
                {this.getProductValidationIcon()}
              </View>
            </View>
          </View>
        </View>

        <View style={styles.field}>
          <View style={styles.description}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>
                Description, commentaires
              </Text>
            </View>
            <TextInput
              ref='description'
              onChangeText={descriptionText => this.setState(prevState => ({...prevState, descriptionText}))}
              multiline={true}
              numberOfLines={5}
              maxLength={512}
              value={this.state.descriptionText}
              style={styles.descriptionInput}
            />
          </View>
        </View>

        <View style={styles.starPicker}>
          <ReviewStarPicker selectedStars={this.state.selectedStars} pushPickedStar={this.setSelectedStars}/>
        </View>

        <View style={styles.field}>
          <View style={styles.imageHeader}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>
                Image
              </Text>
            </View>
            <View style={styles.imagePicker}>
              <TouchableOpacity
                style={this.getNoImagePickedStyle()}
                onPress={this.setNoImagePicked}
              >
                <FontAwesomeIcon icon={faTimesCircle} size={24}/>
              </TouchableOpacity>
              <TouchableOpacity
                style={this.getCameraImagePickedStyle()}
                onPress={this.setCameraPicked}
              >
                <FontAwesomeIcon icon={faCamera} size={24}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <AnimatedCamera
          toValue={ this.state.cameraImagePicked ? Dimensions.get('screen').width : 0 }
          setCapturedImage={this.setCapturedImage}
        />

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('ReviewPublicationPreview', {
            id: uuidv4(),
            product: this.state.productText.trim(),
            description: this.state.descriptionText.trim(),
            stars: this.state.selectedStars,
            image: this.state.capturedImage,
            resetReviewForm: () => this.setState(() => this.defaultState())
          })}
          disabled={this.isPreviewButtonDisabled()}
          style={this.getPreviewButtonStyle()}
        >
          <View style={styles.previewButtonContent}>
            <Text style={this.getPreviewButtonTextStyle()}>Aperçu</Text>
            <FontAwesomeIcon icon={faAngleRight} size={24} style={this.getPreviewButtonIconStyle()}/>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );

  defaultState = (): ReviewPublicationScreenState => ({
      productText: '',
      descriptionText: '',
      selectedStars: 0,
      productInputInvalid: false,
      productInputDirty: false,
      cameraImagePicked: false,
      cameraAllowed: false,
      capturedImage: null,
    })

  setSelectedStars = (i) => this.setState(prevState => ({...prevState, selectedStars: i}));

  setNoImagePicked = () => {
    this.setState( prevState => ({
      ...prevState,
      cameraImagePicked: false,
      capturedImage: null,
    }))
  };

  setCameraPicked = () => {
    this.setState( prevState => ({ ...prevState, cameraImagePicked: true }))
  };

  setCapturedImage = (image: string) => this.setState(() => ({ capturedImage: image }))

  getInputStyle = () => this.state.productInputInvalid
    ? {...styles.productTextInput, ...styles.productInputInvalid}
    : {...styles.productTextInput};

  getProductValidationIcon = () => {
    if (this.state.productInputDirty) {
      return this.state.productInputInvalid
        ? <FontAwesomeIcon icon={faTimesCircle} size={24} color={Colors.errorRed} style={styles.productValidationIcon}/>
        : <FontAwesomeIcon icon={faCheckCircle} size={24} color={Colors.primary} style={styles.productValidationIcon}/>
    }
  };

  handleProductInputChange = (text) => {
    const invalid = !text;
    this.setState(prevState => ({
      ...prevState,
      productText: text,
      productInputDirty: true,
      productInputInvalid: invalid,
    }));
  };

  isPreviewButtonDisabled = () =>
    this.state.productInputInvalid || !this.state.productInputDirty || !this.state.selectedStars;

  getPreviewButtonStyle = () => this.isPreviewButtonDisabled()
    ? {...styles.previewButton, ...styles.previewButtonDisabled}
    : {...styles.previewButton, ...styles.previewButtonEnabled};

  getPreviewButtonTextStyle = () => this.isPreviewButtonDisabled()
    ? {...styles.previewButtonText, ...styles.previewButtonTextDisabled}
    : {...styles.previewButtonText, ...styles.previewButtonTextEnabled};

  getPreviewButtonIconStyle = () => this.isPreviewButtonDisabled()
    ? {...styles.previewButtonTextDisabled}
    : {...styles.previewButtonTextEnabled};

  getNoImagePickedStyle = () => this.state.cameraImagePicked
    ? {...styles.noImageButton, ...styles.noImageNotSelected }
    : {...styles.noImageButton, ...styles.noImageSelected };

  getCameraImagePickedStyle = () => this.state.cameraImagePicked
    ? {...styles.cameraImageButton, ...styles.cameraImageSelected }
    : {...styles.cameraImageButton, ...styles.cameraImageNotSelected };
}

// @ts-ignore
ReviewPublicationScreen.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  reviewPublication: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    color: Colors.black,
    padding: 16,
    paddingTop: 24,
  },
  headerTitle: {
    flex: 3,
    fontSize: 28,
    fontWeight: 'bold',
    justifyContent: 'flex-start',
  },
  headerIcon: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  validationField: {
    flex: 1,
    flexDirection: 'row',
  },
  product: {
    flex: 1,
    flexDirection: 'column',
  },
  labelContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  label: {
    flex: 1,
    color: Colors.darkGray,
    paddingLeft: 8,
    paddingBottom: 4,
    fontSize: 14,
  },
  productText: {
    flex: 5,
    flexDirection: 'row',
  },
  productTextInput: {
    flex: 1,
    height: 32,
    borderRadius: 16,
    paddingLeft: 16,
    backgroundColor: Colors.white,
  },
  productInputInvalid: {
    borderWidth: 1,
    borderColor: Colors.errorRed,
  },
  productValidation: {
    flex: 1,
    flexDirection: 'column-reverse',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  productValidationIcon: {
    marginTop: 4,
  },
  field: {
    margin: 16,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  description: {
    flex: 1,
  },
  descriptionInput: {
    height: 128,
    overflow: 'scroll',
    borderRadius: 12,
    backgroundColor: Colors.white,
    padding: 12,
    paddingTop: 12,
  },
  starPicker: {
    margin: 16,
  },
  previewButton: {
    height: 48,
    width: 192,
    margin: 16,
    marginBottom: 96,
    borderRadius: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  previewButtonEnabled: {
    color: Colors.black,
    backgroundColor: Colors.primary,
  },
  previewButtonDisabled: {
    color: Colors.gray,
    backgroundColor: Colors.lightGray,
  },
  previewButtonContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  previewButtonText: {
    fontSize: 24,
    paddingRight: 12,
    fontWeight: 'bold',
  },
  previewButtonTextDisabled: {
    color: Colors.gray,
  },
  previewButtonTextEnabled: {
    color: Colors.black,
  },
  imageHeader: {
    flex: 1,
    flexDirection: 'column',
  },
  imagePicker: {
    height: 48,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  noImageButton: {
    height: 40,
    width: 56,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noImageSelected: {
    backgroundColor: Colors.errorRed,
  },
  noImageNotSelected: {
    backgroundColor: Colors.white,
  },
  cameraImageButton: {
    height: 40,
    width: 56,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraImageSelected: {
    backgroundColor: Colors.primary,
  },
  cameraImageNotSelected: {
    backgroundColor: Colors.white,
  },
});
