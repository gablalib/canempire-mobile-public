import { faCheckCircle, faGlasses, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import Popup from '../../../components/popup/popup'
import Root from '../../../components/popup/popupRoot'
import React from 'react'
import { ActivityIndicator, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { NavigationStackProp } from 'react-navigation-stack'
import uuidv4 from 'uuid/v4'
import ReviewCard from '../../../components/review/ReviewCard'
import Colors from '../../../constants/Colors'
import { ReviewPublisher } from '../../../services/review/ReviewPublisher'

interface ReviewPublicationPreviewScreenState {
  publishInProgress: boolean
}

interface ReviewPublicationPreviewScreenProps {
  navigation: NavigationStackProp<{
    params: {
      id: string
      product: string
      stars: number
      description: string
      timestamp: number
    }
  }>;
  resetReviewForm: () => void
}

export default class ReviewPublicationPreviewScreen
  extends React.Component<ReviewPublicationPreviewScreenProps, ReviewPublicationPreviewScreenState> {

  private reviewPublisher: ReviewPublisher

  constructor(props: ReviewPublicationPreviewScreenProps) {
    super(props)
    this.reviewPublisher = new ReviewPublisher()
    this.state = {
      publishInProgress: false,
    };
  }

  render() {
    return (
      <Root>
        <View style={styles.header}>
          <Text style={styles.headerTitle}>
            Aperçu
          </Text>
          <FontAwesomeIcon icon={faGlasses} size={28} color={Colors.black} style={styles.headerIcon} />
        </View>

        <ScrollView contentContainerStyle={styles.content}>
          <ReviewCard review={this.props.navigation.state.params}/>

          <View style={styles.bottomButtons}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
              style={styles.cancelButton}
              disabled={this.state.publishInProgress}
            >
              <Text style={styles.cancelButtonText}>Annuler</Text>
              <FontAwesomeIcon icon={faTimesCircle} size={18} color={Colors.white} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.publishReview()}
              style={styles.publishButton}
            >
              <Text style={styles.publishButtonText}>Publier</Text>
              <FontAwesomeIcon icon={faCheckCircle} size={18} color={Colors.black} style={this.getPublishCheckIconStyle().checkIcon} />
              <View style={this.getPublishLoadingIconStyle().loadingIcon}>
                <ActivityIndicator animating={this.state.publishInProgress} />
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Root>
    )
  }

  publishReview = () => {
    this.setState(() => ({ publishInProgress: true }));
    this.reviewPublisher.publishReview({
      reviewId: uuidv4(),
      product: this.props.navigation.getParam('product'),
      description: this.props.navigation.getParam('description'),
      stars: this.props.navigation.getParam('stars'),
      image: this.props.navigation.getParam('image') || '',
      userId: 'canempire-dev', // todo
      timestamp: Date.now()
    })
      .then((response: Response) => response.status >= 200 && response.status < 300 ? this.handlePublishSuccess() : this.handlePublishFailure())
      .catch(this.handlePublishFailure)
  }

  handlePublishSuccess = () => {
    this.setState(() => ({ publishInProgress: false }))
    this.props.navigation.getParam('resetReviewForm')()
    Popup.show({
      type: 'Success',
      title: 'Succès',
      button: false,
      textBody: 'Revue publiée',
      timing: 2000,
      autoClose: true,
      callback: () => this.props.navigation.navigate('ReviewPublication')
    })
  }

  handlePublishFailure = () => {
    this.setState(() => ({ publishInProgress: false }))
    Popup.show({
      type: 'Danger',
      title: 'Erreur',
      button: false,
      textBody: 'Échec de la publication. Réessayez plus tard',
      timing: 3000,
      autoClose: true,
      callback: () => this.props.navigation.navigate('ReviewPublication')
    })
  }

  getPublishCheckIconStyle = () => StyleSheet.create({
    checkIcon: {
      display: this.state.publishInProgress ? 'none' : 'flex'
    }
  })

  getPublishLoadingIconStyle = () => StyleSheet.create({
    loadingIcon: {
      display: this.state.publishInProgress ? 'flex' : 'none'
    }
  })
}

// @ts-ignore
ReviewPublicationPreviewScreen.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
  },
  header: {
    flexDirection: 'row',
    color: Colors.black,
    padding: 16,
    paddingTop: 24,
  },
  headerTitle: {
    flex: 3,
    fontSize: 28,
    fontWeight: 'bold',
    justifyContent: 'flex-start',
  },
  headerIcon: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  content: {
    margin: 8,
    minHeight: 384,
    justifyContent: 'center',
  },
  bottomButtons: {
    height: 64,
    margin: 8,
    marginBottom: 96,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancelButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 24,
    margin: 8,
    height: 48,
    backgroundColor: Colors.errorRed,
  },
  cancelButtonText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.white,
    paddingRight: 4,
  },
  publishButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 24,
    margin: 8,
    height: 48,
    backgroundColor: Colors.primary,
  },
  publishButtonText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.black,
    paddingRight: 4,
  },
});
