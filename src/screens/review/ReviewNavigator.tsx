import React from 'react'
import Colors from '../../constants/Colors';

import { createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import ReviewFeedScreen from './feed/ReviewFeedScreen';
import ReviewPublicationScreen from './publication/ReviewPublicationScreen';
import ReviewPublicationPreviewScreen from './publication/ReviewPublicationPreviewScreen';
import MyReviewsScreen from './my-reviews/MyReviewsScreen';
import TabBarIcon from '../../components/TabBarIcon';
import { faNewspaper, faShareSquare, faUserCircle } from '@fortawesome/free-solid-svg-icons';

const ReviewFeedStack = createStackNavigator({ ReviewFeed: ReviewFeedScreen });

// @ts-ignore
ReviewFeedStack.navigationOptions = {
  tabBarLabel: 'Explorer',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faNewspaper} />
  ),
};

const ReviewPublicationStack = createStackNavigator({
  ReviewPublication: ReviewPublicationScreen,
  ReviewPublicationPreview: ReviewPublicationPreviewScreen,
});

// @ts-ignore
ReviewPublicationStack.navigationOptions = {
  tabBarLabel: 'Publier',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faShareSquare} />
  ),
};

const MyReviewsStack = createStackNavigator({ MyReviews: MyReviewsScreen });

// @ts-ignore
MyReviewsStack.navigationOptions = {
  tabBarLabel: 'Mes revues',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} icon={faUserCircle} />
  ),
};

const ReviewTabNavigator = createBottomTabNavigator({
  ReviewFeedStack,
  ReviewPublicationStack,
  MyReviewsStack,
});

const ReviewNavigator = createSwitchNavigator({ Main: ReviewTabNavigator });

ReviewNavigator.navigationOptions = {
  title: 'Revues SQDC',
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: Colors.navigationBlue,
  headerTitleStyle: {
    fontWeight: 'bold',
    color: Colors.black,
  },
};

export default ReviewNavigator
