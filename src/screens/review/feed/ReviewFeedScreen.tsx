import React from 'react'
import Colors from '../../../constants/Colors'
import { Button } from 'react-native-elements'
import { StyleSheet, TextInput, View } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSearch, faSyncAlt } from '@fortawesome/free-solid-svg-icons'
import ReviewList from '../../../components/review/ReviewList'
import { Review } from '../../../models/review/Review'
import { ReviewExplorer } from '../../../services/review/ReviewExplorer'

type ReviewSort = 'latest' | 'best'

interface ReviewFeedScreenState {
  sortBy: ReviewSort
  reviewList: Review[]
  filterPickerOpen: boolean
  isLoading: boolean
  hasMore: boolean
}

export default class ReviewFeedScreen
  extends React.Component<any, ReviewFeedScreenState> {

  private reviewExplorer: ReviewExplorer;

  constructor(props) {
    super(props)
    this.reviewExplorer = new ReviewExplorer()
    this.state = {
      sortBy: 'latest',
      reviewList: [],
      filterPickerOpen: false,
      isLoading: false,
      hasMore: true,
    }
  }

  componentDidMount = () =>
    this.loadNextReviews()

  render = () =>
    <View style={styles.reviewFeed}>

      <View style={styles.topBar}>
        <View style={styles.searchBarContainer}>
          <View style={styles.searchBar}>
            <FontAwesomeIcon icon={faSearch} size={16} style={styles.searchBarIcon}/>
            <TextInput placeholder={'Rechercher...'} style={styles.searchBarInput} />
          </View>
        </View>

        <View style={styles.filterContainer}>
          <Button
            onPress={this.refreshReviews}
            buttonStyle={styles.filterButton}
            titleStyle={styles.filterButtonText}
            icon={<FontAwesomeIcon icon={faSyncAlt} size={24} style={styles.filterIcon}/>}
          />
        </View>
      </View>

      <ReviewList
        reviews={this.state.reviewList}
        onRefresh={this.refreshReviews}
        onNext={this.loadNextReviews}
        refreshing={this.state.isLoading}
      />
    </View>

  private loadNextReviews = () => {
    if (this.state.hasMore && !this.state.isLoading) {
      this.setState(() => ({ isLoading: true }))
      this.reviewExplorer.loadNext(this.state.reviewList.length)
        .then((reviews: Review[]) => {
          if (reviews.length)
            this.setState(({ reviewList }) => ({ reviewList: reviewList.concat(reviews), isLoading: false }))
          else
            this.setState(() => ({ hasMore: false, isLoading: false }))
        })
    }
  }

  private refreshReviews = () => {
    this.setState(() => ({ isLoading: true }))
    this.reviewExplorer.refresh()
      .then((reviews: Review[]) => {
        this.setState(({reviewList}) => ({ reviewList: reviews.concat(reviewList || []), isLoading: false }))
      })
  }
}

const styles = StyleSheet.create({
  reviewFeed: {
    flex: 1
  },
  topBar: {
    height: 48,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.primary,
    backgroundColor: Colors.white,
    color: Colors.navigationBlue,
  },
  searchBarContainer: {
    flex: 3,
    paddingLeft: 8,
    paddingRight: 8,
  },
  searchBar: {
    backgroundColor: Colors.lightGray,
    borderRadius: 16,
    height: 32,
    flex: 1,
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 8,
  },
  searchBarIcon: {
    marginTop: 8,
    marginLeft: 8,
    color: Colors.darkGray,
  },
  searchBarInput: {
    flex: 1,
    height: 32,
    fontSize: 16,
    marginLeft: 8,
    marginBottom: 0,
    padding: 0,
  },
  filterContainer: {
    flex: 1,
    marginRight: 16,
    color: Colors.navigationBlue,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  filterButton: {
    borderWidth: 0,
    backgroundColor: Colors.white,
  },
  filterIcon: {
    color: Colors.navigationBlue,
  },
  filterButtonText: {
    color: Colors.navigationBlue,
  }
});

// @ts-ignore
ReviewFeedScreen.navigationOptions = {
  headerShown: false,
};
