import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native'
import Colors from '../../constants/Colors';
import { Review } from '../../models/review/Review'
import ReviewCardStars from './ReviewCardStars';

interface ReviewCardProps {
  review: Review
}

const DEFAULT_IMAGE_SIZE = Dimensions.get('screen').width - 20 // - 20 to account for margins (2x8) and border (2x2) sizes

export default class ReviewCard
  extends React.Component<ReviewCardProps, any> {
  render = () =>
    <View style={styles.card}>
      <View style={styles.header}>
        <View style={styles.headerTitle}>
          <Text style={styles.headerTitleText}>{this.props.review.product}</Text>
        </View>
        <View style={styles.headerStarsContainer}>
          <ReviewCardStars stars={this.props.review.stars}/>
        </View>
      </View>

      {
        this.props.review.description ?
          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionText}>
              {this.props.review.description}
            </Text>
          </View>
          : <View/>
      }
      {
        this.props.review.image ?
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={{uri: `data:image/gif;base64, ${this.props.review.image}`}}
            />
          </View>
          : <View/>
      }
    </View>
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    borderWidth: 2,
    borderColor: Colors.gray,
    backgroundColor: Colors.white,
    marginBottom: 8,
  },
  header: {
    minHeight: 48,
    padding: 8,
    flex: 1,
    flexDirection: 'row',
  },
  headerTitle: {
    flex: 1,
    justifyContent: 'center',
  },
  headerTitleText: {
    fontSize: 16,
    fontWeight: 'bold',
    justifyContent: 'flex-start',
  },
  headerStarsContainer: {
    width: 128,
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  descriptionContainer: {
    textAlign: 'left',
    padding: 8,
    borderTopWidth: 1,
    borderTopColor: Colors.lightGray,
  },
  descriptionText: {
    fontSize: 14,
  },
  imageContainer: {
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
    overflow: 'hidden',
  },
  image: {
    width: DEFAULT_IMAGE_SIZE,
    height: DEFAULT_IMAGE_SIZE,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
    overflow: 'hidden',
  }
});
