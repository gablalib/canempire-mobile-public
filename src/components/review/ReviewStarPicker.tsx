import React from 'react'
import { StyleSheet, View, PanResponder, PanResponderInstance } from 'react-native'
import { faCannabis } from '@fortawesome/free-solid-svg-icons/index';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import Layout from '../../constants/Layout'
import Colors from '../../constants/Colors';

interface ReviewStarPickerProps {
  selectedStars: number
  pushPickedStar: (i: number) => void
}

interface ReviewStarPickerState {
  stars: number
}

export default class ReviewStarPicker
  extends React.Component<ReviewStarPickerProps, ReviewStarPickerState> {

  private _panResponder: PanResponderInstance = null

  constructor(props) {
    super(props);

    this.state = { stars: props.selectedStars };
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onStartShouldSetPanResponderCapture: () => true,
      onMoveShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onShouldBlockNativeResponder: () => true,

      // I have no clue why this works
      onPanResponderGrant: (e) => this.setStars(e.nativeEvent.pageX - (Layout.window.width - styles.starsContainer.width)),
      onPanResponderMove: (e) => this.setStars(e.nativeEvent.pageX - (Layout.window.width - styles.starsContainer.width)),
    });
  }

  componentDidUpdate(): void {
    if (!this.props.selectedStars && !!this.state.stars)
      this.setState(() => ({ stars: 0 }))
  }

  render() {
    return (
      <View style={styles.starsContainer} {...this._panResponder.panHandlers}>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={40} color={this.state.stars >= 1 ? Colors.darkGreen : Colors.white}/>
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={40} color={this.state.stars >= 2 ? Colors.darkGreen : Colors.white}/>
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={40} color={this.state.stars >= 3 ? Colors.darkGreen : Colors.white}/>
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={40} color={this.state.stars >= 4 ? Colors.darkGreen : Colors.white}/>
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={40} color={this.state.stars >= 5 ? Colors.darkGreen : Colors.white}/>
        </View>
      </View>
    )
  }

  getStarContainerStyle = () => {
    switch(this.state.stars) {
      case 0:
        return { ...styles.starContainer, ...styles.zeroStarContainer };
      case 1:
        return { ...styles.starContainer, ...styles.oneStarContainer };
      case 2:
        return { ...styles.starContainer, ...styles.twoStarsContainer };
      case 3:
        return { ...styles.starContainer, ...styles.threeStarsContainer };
      case 4:
        return { ...styles.starContainer, ...styles.fourStarsContainer };
      case 5:
      default:
        return { ...styles.starContainer, ...styles.fiveStarsContainer };
    }
  };

  setStars = (xPosition) => {
    const width = styles.starsContainer.width;

    if (xPosition <= 0) {
      this.setState({ stars: 1 });
      this.props.pushPickedStar(1)
    }
    else if (xPosition <= (width) / 5) {
      this.setState({ stars: 2 });
      this.props.pushPickedStar(2)
    }
    else if (xPosition <= (2*width) / 5) {
      this.setState({ stars: 3 });
      this.props.pushPickedStar(3)
    }
    else if (xPosition <= (3*width) / 5) {
      this.setState({ stars: 4 });
      this.props.pushPickedStar(4)
    }
    else {
      this.setState({ stars: 5 });
      this.props.pushPickedStar(5)
    }
  };
}

const styles = StyleSheet.create({
  starsContainer: {
    width: 256,
    height: 56,
    borderRadius: 28,
    backgroundColor: Colors.gray,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer: {
    width: 46,
    height: 46,
    marginLeft: 2,
    marginRight: 2,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  zeroStarContainer: {
    backgroundColor: Colors.lightGray,
  },
  oneStarContainer: {
    backgroundColor: Colors.lightRed,
  },
  twoStarsContainer: {
    backgroundColor: Colors.lightOrange,
  },
  threeStarsContainer: {
    backgroundColor: Colors.lightYellowGreen,
  },
  fourStarsContainer: {
    backgroundColor: Colors.lightGreen,
  },
  fiveStarsContainer: {
    backgroundColor: Colors.lightTurquoise,
  },
});
