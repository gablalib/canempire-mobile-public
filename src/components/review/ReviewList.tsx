import React from 'react'
import { FlatList, StyleSheet } from 'react-native'
import { Review } from '../../models/review/Review'
import ReviewCard from './ReviewCard'

interface ReviewListProps {
  reviews: Review[]
  onRefresh: () => void
  onNext: () => void
  refreshing: boolean
}

export default class ReviewList extends React.Component<ReviewListProps, any> {
  render = () =>
      <FlatList
        data={this.props.reviews}
        renderItem={({ item }) => <ReviewCard review={item}/>}
        keyExtractor={review => review.id}
        contentContainerStyle={styles.reviewsContainer}
        refreshing={this.props.refreshing}
        onRefresh={this.props.onRefresh}
        onEndReached={this.props.onNext}
        onEndReachedThreshold={0.5}
      />
}

const styles = StyleSheet.create({
  reviewsContainer: {
    flex: 0,
    margin: 8,
  },
  endContainer: {
    height: 48,
    alignItems: 'center',
  },
});
