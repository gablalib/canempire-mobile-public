import React from 'react'
import { StyleSheet, View } from 'react-native'
import Colors from '../../constants/Colors';
import { faCannabis } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

interface ReviewCardStarsProps {
  stars: number
}

export default class ReviewCardStars
  extends React.Component<ReviewCardStarsProps, any> {

  render = () =>
      <View style={styles.starsContainer}>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={20} color={Colors.darkGreen} />
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={20} color={this.props.stars >= 2 ? Colors.darkGreen : Colors.white} />
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={20} color={this.props.stars >= 3 ? Colors.darkGreen : Colors.white} />
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={20} color={this.props.stars >= 4 ? Colors.darkGreen : Colors.white} />
        </View>
        <View style={this.getStarContainerStyle()}>
          <FontAwesomeIcon icon={faCannabis} size={20} color={this.props.stars >= 5 ? Colors.darkGreen : Colors.white} />
        </View>
      </View>

  getStarContainerStyle = () => {
    switch(this.props.stars) {
      case 1:
        return { ...styles.starContainer, ...styles.oneStarContainer };
      case 2:
        return { ...styles.starContainer, ...styles.twoStarsContainer };
      case 3:
        return { ...styles.starContainer, ...styles.threeStarsContainer };
      case 4:
        return { ...styles.starContainer, ...styles.fourStarsContainer };
      case 5:
      default:
        return { ...styles.starContainer, ...styles.fiveStarsContainer };
    }
  };
}

const styles = StyleSheet.create({
  starsContainer: {
    width: 128,
    height: 28,
    borderRadius: 14,
    backgroundColor: Colors.gray,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer: {
    width: 22,
    height: 22,
    marginLeft: 1,
    marginRight: 1,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  oneStarContainer: {
    backgroundColor: Colors.lightRed,
  },
  twoStarsContainer: {
    backgroundColor: Colors.lightOrange,
  },
  threeStarsContainer: {
    backgroundColor: Colors.lightYellowGreen,
  },
  fourStarsContainer: {
    backgroundColor: Colors.lightGreen,
  },
  fiveStarsContainer: {
    backgroundColor: Colors.lightTurquoise,
  },
});
