import React from 'react'
import { Animated, Easing, View, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

interface RightSlideAnimationProps {
  msDelay: number
}

export class RightSlideAnimation
  extends React.Component<RightSlideAnimationProps, any> {

  private xSlide: Animated.Value = new Animated.Value(0)

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    Animated.spring(this.xSlide,{
      delay: this.props.msDelay ? this.props.msDelay : 0,
      toValue: 1,
      useNativeDriver: true,
    }).start();
  }

  render = () => (
    <Animated.View
      style={{
        transform: [{
          translateX: this.xSlide.interpolate({
            inputRange: [0, 1],
            outputRange: [-width, 0]
          })
        }]
      }}
    >
      <View>{this.props.children}</View>
    </Animated.View>
  )
}
