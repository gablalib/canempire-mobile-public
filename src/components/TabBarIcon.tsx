import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import Colors from '../constants/Colors';

interface TabBarIconProps {
  icon: any
  focused: boolean
}

export default class TabBarIcon
  extends React.Component<TabBarIconProps, any> {
  render = () =>
    <FontAwesomeIcon
      icon={this.props.icon}
      size={24}
      style={{
        color: this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault,
      }}
    />
}
