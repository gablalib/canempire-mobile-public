import { faCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { Camera } from 'expo-camera'
import { CapturedPicture } from 'expo-camera/src/Camera.types'
import * as ImageManipulator from 'expo-image-manipulator'
import { SaveFormat } from 'expo-image-manipulator'
import React from 'react'
import { Animated, Dimensions, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import Colors from '../constants/Colors'

interface AnimatedCameraProps {
  toValue: number,
  setCapturedImage: (image: string | null) => void
}

interface AnimatedCameraState {
  animatedHeight: Animated.Value,
  image: string | null
}

export class AnimatedCamera extends React.Component<AnimatedCameraProps, AnimatedCameraState> {

  private camera: Camera = null

  constructor(props) {
    super(props)
    this.state = {
      animatedHeight: new Animated.Value(0),
      image: null
    }

    Animated.timing(this.state.animatedHeight, {
      toValue: this.props.toValue,
      duration: 300,
      useNativeDriver: true,
    }).start()
  }

  render = () =>
    this.props.toValue
      ? <Animated.View
        style={{ height: this.state.animatedHeight, width: Dimensions.get('screen').width, backgroundColor: Colors.gray }}
      >
        {this.state.image ? this.renderImage() : this.renderCamera()}
      </Animated.View>
      : <View/>

  renderCamera = () =>
    <Camera style={styles.container} ref={ref => this.camera = ref}>

      <View style={styles.bottomCameraBar}>
        <TouchableOpacity style={styles.captureButton} onPress={this.captureImage}>
          <FontAwesomeIcon icon={faCircle} size={48} color={Colors.white}/>
        </TouchableOpacity>
      </View>
    </Camera>

  renderImage = () =>
    <View style={styles.container}>
      <View style={styles.topImageBar}>
        <TouchableOpacity style={styles.deleteCaptureButton} onPress={this.deleteCapturedImage}>
          <FontAwesomeIcon icon={faTimesCircle} size={36} color={Colors.white}/>
        </TouchableOpacity>
      </View>
      <Image
        style={{ width: this.props.toValue, height: this.props.toValue }}
        source={{uri: `data:image/gif;base64, ${this.state.image}`}}
      />
    </View>

  captureImage = () => {
    this.camera.takePictureAsync({
      quality: 0,
      base64: true,
      onPictureSaved: (image: CapturedPicture) => {
        ImageManipulator.manipulateAsync(image.uri,
          [{ resize: { width: 500, height: 500 } }], { format: SaveFormat.PNG, base64: true })
          .then((resizedImage) => {
            this.setState(() => ({ image: resizedImage.base64 }))
            this.props.setCapturedImage(resizedImage.base64)
          })
      }
    })
  }

  deleteCapturedImage = () => {
    this.setState(() => ({ image: null }))
    this.props.setCapturedImage(null)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topImageBar: {
    flex: 1,
    zIndex: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  bottomCameraBar: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 12,
  },
  captureButton: {
    width: 56,
    height: 56,
    borderRadius: 28,
    backgroundColor: Colors.primary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  deleteCaptureButton: {
    width: 36,
    height: 36,
    borderRadius: 18,
    backgroundColor: Colors.errorRed,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 12,
    marginRight: 12,
  }
})
