import React from 'react'
import { ActivityIndicator } from 'react-native'
import Colors from '../constants/Colors';

export const LoadingSpinner = () =>
  <ActivityIndicator size='large' color={Colors.gray} animating={true} />;
