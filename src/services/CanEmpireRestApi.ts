
export abstract class CanEmpireRestApi {
  private apiEndpoint = 'http://localhost:8080'

  protected latestReviewsEndpoint = `${this.apiEndpoint}/reviews/latest`
  protected bestReviewsEndpoint = `${this.apiEndpoint}/reviews/best`
  protected publishReviewEndpoint = `${this.apiEndpoint}/reviews/publish`
  protected newReviewsEndpoint = `${this.apiEndpoint}/reviews/new`
}
