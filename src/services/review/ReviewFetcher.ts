import { Review } from '../../models/review/Review'
import { CanEmpireRestApi } from '../CanEmpireRestApi'

export interface GetReviewsResponse {
  lastUpdated: number
  reviews: Review[]
}

export class ReviewFetcher extends CanEmpireRestApi {

  public getLatest = (amount: number, offset: number, lastUpdated: number): Promise<GetReviewsResponse> =>
    this.fetchLatest(JSON.stringify({ amount, offset, lastUpdated }))
      .then(response => response.json())

  public getNewSince = (timestamp: number): Promise<GetReviewsResponse> =>
    this.fetchNew(JSON.stringify({ since: timestamp }))
      .then(response => response.json())

  private fetchLatest = (requestBody: string): Promise<Response> =>
    fetch(this.latestReviewsEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: requestBody
    })

  private fetchNew = (requestBody: string): Promise<Response> =>
    fetch(this.newReviewsEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: requestBody
    })

}
