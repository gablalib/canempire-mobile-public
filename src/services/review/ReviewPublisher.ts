import { PublishReviewRequest } from '../../models/review/PublishReviewRequest'
import { CanEmpireRestApi } from '../CanEmpireRestApi'

export class ReviewPublisher extends CanEmpireRestApi {

  public publishReview = (review: PublishReviewRequest): Promise<Response> =>
    fetch(this.publishReviewEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify(review)
    })
}

