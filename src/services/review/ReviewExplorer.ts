import { Review } from '../../models/review/Review'
import { GetReviewsResponse, ReviewFetcher } from './ReviewFetcher'

const DEFAULT_FETCHED_AMOUNT = 10

export class ReviewExplorer {

  private fetcher: ReviewFetcher
  private lastUpdated: number = Date.now()

  constructor() {
    this.fetcher = new ReviewFetcher()
  }

  loadNext = (offset: number): Promise<Review[]> =>
    this.fetcher.getLatest(DEFAULT_FETCHED_AMOUNT, offset, this.lastUpdated)
      .then((response: GetReviewsResponse) => {
        this.lastUpdated = response.lastUpdated
        return response.reviews ? response.reviews : []
      })

  refresh = (): Promise<Review[]> =>
    this.fetcher.getNewSince(this.lastUpdated)
      .then((response: GetReviewsResponse) => {
        this.lastUpdated = response.lastUpdated
        return response.reviews ? response.reviews : []
      })

}
