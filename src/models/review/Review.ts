
export interface Review {
  id: string
  product: string
  description?: string
  timestamp: number
  stars: number
  image?: string
  userId?: string
  likes?: string[]
  dislikes?: string[]
  likesCount?: number
  dislikesCount?: number
}
