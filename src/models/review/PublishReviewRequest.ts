
export interface PublishReviewRequest {
  reviewId: string
  userId: string
  product: string
  description?: string
  stars: number
  timestamp: number
  image?: string
}
