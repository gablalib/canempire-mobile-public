import React from 'react';
import { AppNavigator } from './AppNavigator';
import { createAppContainer } from 'react-navigation';

// @ts-ignore
export default createAppContainer(AppNavigator);
