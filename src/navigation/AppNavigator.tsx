import ReviewNavigator from '../screens/review/ReviewNavigator';
import HomeScreen from '../screens/HomeScreen';
import ContactScreen from '../screens/contact/ContactScreen';
import SocialScreen from '../screens/social/SocialScreen';
import CanfestNavigator from '../screens/canfest/CanfestNavigator';
import { createStackNavigator } from 'react-navigation-stack';

export const AppNavigator = createStackNavigator({
  Home: { screen: HomeScreen },
  Review: { screen: ReviewNavigator },
  Canfest: { screen: CanfestNavigator },
  Social: { screen: SocialScreen },
  Contact: { screen: ContactScreen },
});
